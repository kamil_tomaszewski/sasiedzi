Do uruchomienia projektu potrzebna jest java w wersji 17, ustawiona jako nomyśna wersja w systemie
oraz nodejs w wersji min v12.14 + npm

Aby uruchomić backed wykonaj polecenie w głównym katalogu:
   
    gradlew bootRun

Aby uruchomić frontend w katalogu front/sasiedzi uruchom polecenia

    npm install
    npm install -g @angular/cli
    ng serve --proxy-config proxy.conf.json


dostępni użytkownicy znajdują sie w src/main/resources/users.json, 
aby przelogować się na innego użytkownika zmień w przeglądarce wartoć ciasteczka loggedUser na id wybranego użytkownika
użytkownik 110 ma podwyższone uprawnienia 