import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppRootViewComponent } from './app-root-view.component';

describe('AppRootViewComponent', () => {
  let component: AppRootViewComponent;
  let fixture: ComponentFixture<AppRootViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppRootViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppRootViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
