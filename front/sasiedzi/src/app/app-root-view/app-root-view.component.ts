import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-root-view',
  templateUrl: './app-root-view.component.html',
  styleUrls: ['./app-root-view.component.scss']
})
export class AppRootViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
