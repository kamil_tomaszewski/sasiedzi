import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {PeopleDataServiceService} from 'services/people-data-service.service';
import {SharedService} from 'services/shared.service';
import {CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private translate: TranslateService,
              private sharedService: SharedService,
              private peopleDataService: PeopleDataServiceService,
              private cookieService: CookieService) {
    translate.setDefaultLang('pl');
  }

  ngOnInit(): void {
    const loggedUserId = this.cookieService.get('loggedUser');
    if (!loggedUserId) {
      this.cookieService.set('loggedUser', "101");
    }
    this.sharedService.loggedUser = this.peopleDataService.findPersonById(Number(loggedUserId));
  }
}
