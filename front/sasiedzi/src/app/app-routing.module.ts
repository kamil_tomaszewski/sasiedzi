import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventsResolverService } from 'services/events-resolver.service';
import { AppRootViewComponent } from './app-root-view/app-root-view.component';
import { AppComponent } from './app.component';
import {PeoplesResolverService} from 'services/peoples-resolver.service';
import {EquipmentsResolverService} from 'services/equipments-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    resolve: {},
    children: [
      {
        path: '', component: AppRootViewComponent,
        children: [
          { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
          { path: '', redirectTo: 'people', pathMatch: 'full' },
          {
            path: 'people',
            resolve: {
              events: PeoplesResolverService
            },
            loadChildren: () => import('modules/people/people.module').then((mod) => mod.PeopleModule)
          }, {
            path: 'dashboard',
            resolve: {

            },
            loadChildren: () => import('modules/dashboard/dashboard.module').then((mod) => mod.DashboardModule)
          }, {
            path: 'events',
            resolve: {
              events: EventsResolverService
            },
            loadChildren: () => import('modules/event/event.module').then((mod) => mod.EventModule)
          }, {
            path: 'equipments',
            resolve: {
              events: EquipmentsResolverService
            },
            loadChildren: () => import('modules/equipment/equipment.module').then((mod) => mod.EquipmentModule)
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
