import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {EquipmentDataService} from './equipment-data.service';
import {EquipmentHttpService} from './equipment-http.service';

@Injectable({
  providedIn: 'root'
})
export class EquipmentsResolverService {
  constructor(
    private equipmentDataService: EquipmentDataService,
    private equipmentHttp: EquipmentHttpService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.equipmentHttp.getEquipment();
  }
}
