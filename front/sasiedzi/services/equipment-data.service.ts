import {Injectable} from '@angular/core';
import {Equipment} from "../interfaces/equipment";
import {EquipmentHttpService} from "./equipment-http.service";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class EquipmentDataService {

  public equipmentList: Equipment[] | null = [];

  constructor(
    private equipmentHttpService: EquipmentHttpService) {
    this.equipmentList = this.loadEventsFromCache();
  }

  findEquipmentIdById(equipmentId: number): Equipment | undefined {
    if (this.equipmentList) {
      const equipment = this.equipmentList.find((equipment) => {
        return equipment.id === equipmentId
      });

      console.info('equipment', equipment);
      return equipment;
    }
    return undefined;
  }

  loadEventsFromCache(): Equipment[] | null {
    const localData = localStorage.getItem('equipments');

    if (localData) {
      return JSON.parse(localData);
    }

    return null
  }

  acceptEquipment(equipmentId: number): Observable<Equipment[]> {
    return this.equipmentHttpService.acceptEquipment(equipmentId);
  }

  returnEquipment(equipmentId: number): Observable<Equipment[]> {
    return this.equipmentHttpService.returnEquipment(equipmentId);
  }

  returnSuperuserEquipment(equipmentId: number): Observable<Equipment[]> {
    return this.equipmentHttpService.returnSuperuserEquipment(equipmentId);
  }

  assign(equipmentId: number, userId: number): Observable<Equipment[]> {
    return this.equipmentHttpService.assign(equipmentId, userId)
  }

  // assign(equipmentId: number, userId: number) : Observable<Equipment[]> {
  //   return  this.equipmentHttpService.assign(equipmentId, userId).$promise
  // }


}
