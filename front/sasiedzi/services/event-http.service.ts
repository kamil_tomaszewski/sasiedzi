import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CompanyEvent } from 'interfaces/event';
import { Person } from 'interfaces/person';
import { Observable } from 'rxjs';
import {AddEventCommand} from '../interfaces/add-event-command';
import {UpdateEventCommand} from '../interfaces/update-event-command';

@Injectable({
  providedIn: 'root'
})
export class EventHttpService {

  private baseUrl = `/api`;

  constructor(
    private httpClient: HttpClient) {
  }

  public getEvent(): Observable<CompanyEvent[]> {
    return this.httpClient.get<CompanyEvent[]>(`${this.baseUrl}/event`);
  }

  public createEvent(command: AddEventCommand): Observable<CompanyEvent[]> {
    return this.httpClient.post<any>(`${this.baseUrl}/event`, command);
  }

  public updateEvent(command: UpdateEventCommand): Observable<CompanyEvent[]> {
    return this.httpClient.put<CompanyEvent[]>(`${this.baseUrl}/event`, command);
  }

  public acceptEvent(eventId: number): Observable<CompanyEvent[]> {
    return this.httpClient.put<CompanyEvent[]>(`${this.baseUrl}/event/${eventId}/accept`, {});
  }

  public addUserToEvent(eventId: number, userId: number): Observable<CompanyEvent[]> {
    return this.httpClient.put<CompanyEvent[]>(`${this.baseUrl}/event/${eventId}/user/${userId}`,{});
  }

  public removeUserFromEvent(eventId: number, userId: number): Observable<CompanyEvent[]> {
    return this.httpClient.delete<CompanyEvent[]>(`${this.baseUrl}/event/${eventId}/user/${userId}`);
  }

  public addCommentToEvent(eventId: number, userId: number, commentValue: string): Observable<CompanyEvent[]> {
    return this.httpClient.post<CompanyEvent[]>(`${this.baseUrl}/event/${eventId}/comment`, {userId, value: commentValue});
  }

  public updateCommentFromEvent(eventId: number, commentId: number, commentValue: string): Observable<CompanyEvent[]> {
    return this.httpClient.put<CompanyEvent[]>(`${this.baseUrl}/event/${eventId}/comment`, {commentId, value: commentValue});
  }

  public deleteCommentFromEvent(eventId: number, commentId: number): Observable<CompanyEvent[]> {
    return this.httpClient.put<CompanyEvent[]>(`${this.baseUrl}/event/${eventId}/comment/${commentId}`,{});
  }

  public delete(eventId: number): Observable<CompanyEvent[]> {
    return this.httpClient.put<CompanyEvent[]>(`${this.baseUrl}/event/${eventId}`,{});
  }
}
