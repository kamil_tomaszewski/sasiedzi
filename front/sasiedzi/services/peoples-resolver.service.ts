import {ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {PeopleDataServiceService} from './people-data-service.service';
import {PeopleHttpService} from './people-http.service';

@Injectable({
  providedIn: 'root'
})
export class PeoplesResolverService {
  constructor(
    private peopleDataService: PeopleDataServiceService,
    private peopleHttp: PeopleHttpService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return this.peopleHttp.getUsers();
  }
}
