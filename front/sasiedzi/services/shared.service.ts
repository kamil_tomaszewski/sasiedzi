import { Injectable } from '@angular/core';
import { Person } from 'interfaces/person';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  public loggedUser: Person | undefined;
  constructor() { }
}
