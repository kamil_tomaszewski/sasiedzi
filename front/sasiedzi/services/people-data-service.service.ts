import { Injectable } from '@angular/core';
import { Person } from 'interfaces/person';

@Injectable({
  providedIn: 'root'
})


export class PeopleDataServiceService {

  public peopleList: Person[] | null = [];

  constructor() {
    this.peopleList = this.loadPeopleFromCache();
  }

  findPersonById(personId: number): Person | undefined {
    if (this.peopleList) {
      const personDetails = this.peopleList.find((person) => {
        return person.id === personId
      });

      console.info(personDetails);
      return personDetails;
    }
    return undefined;
  }

  loadPeopleFromCache(): Person[] | null {
    const localData = localStorage.getItem('people');

    if (localData) {
      return JSON.parse(localData);
    }

    return null
  }
}
