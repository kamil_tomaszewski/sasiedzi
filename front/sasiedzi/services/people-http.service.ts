import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Person } from 'interfaces/person';
import { Observable } from 'rxjs';
import { CreateUserCommand } from '../interfaces/create-user-command';

@Injectable({
  providedIn: 'root'
})
export class PeopleHttpService {

  private baseUrl = `/api`;

  constructor(
    private httpClient: HttpClient) {
  }

  public getUsers(): Observable<Person[]> {
    return this.httpClient.get<Person[]>(`${this.baseUrl}/user`);
  }

  public saveUser(command: CreateUserCommand): Observable<Person> {
    return this.httpClient.post<Person>(`${this.baseUrl}/user`, { command });
  }

  public removeUser(userId: number): Observable<Person> {
    return this.httpClient.delete<Person>(`${this.baseUrl}/user/${userId}`);
  }
}
