import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { EventDataService } from './event-data.service';
import { EventHttpService } from './event-http.service';

@Injectable({
  providedIn: 'root'
})
export class EventsResolverService {

  constructor(
    private eventDataService: EventDataService,
    private eventHttp: EventHttpService) {
  }

  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
            return this.eventHttp.getEvent();
  }
}
