import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AddEquipmentCommand} from '../interfaces/add-equipment-command';
import {UpdateEquipmentCommand} from '../interfaces/update-equipment-command';
import {Equipment} from "../interfaces/equipment";

@Injectable({
  providedIn: 'root'
})
export class EquipmentHttpService {

  private baseUrl = `/api`;

  constructor(
    private httpClient: HttpClient) {
  }

  public getEquipment(): Observable<Equipment[]> {
    return this.httpClient.get<Equipment[]>(`${this.baseUrl}/equipment`);
  }

  public getEquipmentForUser(userId: number): Observable<{ myEquipments: Equipment[], otherEquipments: Equipment[] }> {
    return this.httpClient.get<{ myEquipments: Equipment[], otherEquipments: Equipment[] }>(`${this.baseUrl}/equipment/user/${userId}`);
  }

  public createEquipment(command: AddEquipmentCommand): Observable<Equipment[]> {
    return this.httpClient.post<Equipment[]>(`${this.baseUrl}/equipment`, command);
  }

  public updateEquipment(command: UpdateEquipmentCommand): Observable<Equipment[]> {
    return this.httpClient.put<Equipment[]>(`${this.baseUrl}/equipment`, command);
  }

  public acceptEquipment(equipmentId: number): Observable<Equipment[]> {
    return this.httpClient.put<Equipment[]>(`${this.baseUrl}/equipment/${equipmentId}/accept`, {});
  }

  public assign(equipmentId: number, userId: number): Observable<Equipment[]> {
    return this.httpClient.put<Equipment[]>(`${this.baseUrl}/equipment/${equipmentId}/user/${userId}`, {});
  }

  public returnEquipment(equipmentId: number): Observable<Equipment[]> {
    return this.httpClient.put<Equipment[]>(`${this.baseUrl}/equipment/${equipmentId}/return`, {});
  }

  public returnSuperuserEquipment(equipmentId: number): Observable<Equipment[]> {
    return this.httpClient.put<Equipment[]>(`${this.baseUrl}/equipment/${equipmentId}/return_superuser`, {});
  }
}
