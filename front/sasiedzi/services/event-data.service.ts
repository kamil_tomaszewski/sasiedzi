import { Injectable } from '@angular/core';
import { CompanyEvent } from 'interfaces/event';
import { EventHttpService } from './event-http.service';
import {Observable} from 'rxjs';
import {AddEventCommand} from '../interfaces/add-event-command';

@Injectable({
  providedIn: 'root'
})
export class EventDataService {

  public eventList: CompanyEvent[] | null = [];

  constructor(
    private eventHttpService: EventHttpService) {
    this.eventList = this.loadEventsFromCache();
  }

  findEventById(eventId: number): CompanyEvent | undefined {
    if (this.eventList) {
      const eventDetails = this.eventList.find((event) => {
        return event.id === eventId
      });

      console.info('eventDetails', eventDetails);
      return eventDetails;
    }
    return undefined;
  }

  loadEventsFromCache(): CompanyEvent[] | null {
    const localData = localStorage.getItem('companyEvents');

    if (localData) {
      return JSON.parse(localData);
    }

    return null
  }

  addUserToEvent(eventId: number, userId: number): Observable<any> {
   return this.eventHttpService.addUserToEvent(eventId, userId)
  }

  removeUserFromEvent(eventId: number, userId: number): Observable<any> {
    return this.eventHttpService.removeUserFromEvent(eventId, userId)
  }

  createEvent(command: AddEventCommand): Observable<any> {
    return this.eventHttpService.createEvent(command)
  }

  addCommentToEvent(eventId: number, userId: number, commentValue: string) {
    this.eventHttpService.addCommentToEvent(eventId, userId, commentValue).subscribe((data: CompanyEvent[]) => {
      this.eventList = data;
    })
  }

  updateCommentFromEvent(eventId: number, userId: number, commentValue: string) {
    this.eventHttpService.updateCommentFromEvent(eventId, userId, commentValue).subscribe((data: CompanyEvent[]) => {
      this.eventList = data;
    })
  }

  deleteCommentFromEvent(eventId: number, userId: number) {
    this.eventHttpService.deleteCommentFromEvent(eventId, userId).subscribe((data: CompanyEvent[]) => {
      this.eventList = data;
    })
  }

}
