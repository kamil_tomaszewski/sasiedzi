import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EquipmentRootViewComponent } from './equipment-root-view.component';

describe('EquipmentRootViewComponent', () => {
  let component: EquipmentRootViewComponent;
  let fixture: ComponentFixture<EquipmentRootViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EquipmentRootViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EquipmentRootViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
