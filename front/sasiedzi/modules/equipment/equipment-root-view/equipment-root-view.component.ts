import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {EquipmentDataService} from '../../../services/equipment-data.service';

@Component({
  selector: 'app-equipment-root-view',
  templateUrl: './equipment-root-view.component.html',
  styleUrls: ['./equipment-root-view.component.scss']
})
export class EquipmentRootViewComponent implements OnInit {

  routeDataSubscribe: Subscription = new Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private equipmentDataService: EquipmentDataService) { }

  ngOnInit(): void {
    this.routeDataSubscribe = this.activatedRoute.data.subscribe((data) => {
      this.equipmentDataService.equipmentList = data.events;
      localStorage.setItem('equipments', JSON.stringify(data));
    });
  }

}
