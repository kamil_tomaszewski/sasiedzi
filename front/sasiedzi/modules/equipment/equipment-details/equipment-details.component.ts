import {Component, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {Equipment} from "../../../interfaces/equipment";
import {EquipmentDataService} from "../../../services/equipment-data.service";
import {SharedService} from "../../../services/shared.service";
import {Person} from "../../../interfaces/person";
import {PeopleHttpService} from "../../../services/people-http.service";

@Component({
  selector: 'app-equipment-details',
  templateUrl: './equipment-details.component.html',
  styleUrls: ['./equipment-details.component.scss']
})
export class EquipmentDetailsComponent implements OnInit {

  routeDataSubscribe: Subscription = new Subscription;
  equipmentId: number | null;
  equipmentDetails: Equipment | undefined;
  peopleList: Person[] = [];
  loggedUser: Person | undefined;
  owner: Person | undefined;
  selectPeople: number | undefined;

  constructor(
    private equipmentDataService: EquipmentDataService,
    private activatedRoute: ActivatedRoute,
    private peopleHttpService: PeopleHttpService,
    private sharedService: SharedService) {
    this.equipmentId = null;
  }

  ngOnInit(): void {
    this.loggedUser = this.sharedService.loggedUser
    this.routeDataSubscribe = this.activatedRoute.paramMap.subscribe(paramMap => {
      // @ts-ignore - params exist in paramMap
      console.info('equipmentId', paramMap.params.equipmentId);
      // @ts-ignore - params exist in paramMap
      this.equipmentId = Number(paramMap.params.equipmentId);
      this.equipmentDetails = this.equipmentDataService.findEquipmentIdById(this.equipmentId);
    });
    this.peopleHttpService.getUsers().subscribe((data: Person[]) => {
      console.info(data);
      localStorage.setItem('people', JSON.stringify(data));
      this.peopleList = data;
      this.owner = this.peopleList.find(a => a.id == this.equipmentDetails?.currentOwnerId)!;
    })
  }

  accept(equipmentId: number) {
    this.equipmentDataService.acceptEquipment(equipmentId).subscribe((data: Equipment[]) => {
      this.updateData(data);
    });
  }

  returnEquipment(equipmentId: number) {
    this.equipmentDataService.returnEquipment(equipmentId).subscribe((data: Equipment[]) => {
      this.updateData(data);
    });
  }

  returnSuperuserEquipment(equipmentId: number) {
    this.equipmentDataService.returnSuperuserEquipment(equipmentId).subscribe((data: Equipment[]) => {
      this.updateData(data);
    });
  }

  assign(equipmentId: number) {
    this.equipmentDataService.assign(equipmentId, this.selectPeople!).subscribe((data: Equipment[]) => {
      this.updateData(data);
    });
  }

  private updateData(data: Equipment[]) {
    this.equipmentDataService.equipmentList = data;
    this.equipmentDetails = this.equipmentDataService.findEquipmentIdById(this.equipmentId!);
    this.owner = this.peopleList.find(a => a.id == this.equipmentDetails?.currentOwnerId)!;
  }
}
