import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EquipmentListComponent} from './equipment-list/equipment-list.component';
import {EquipmentDetailsComponent} from "./equipment-details/equipment-details.component";
import {EquipmentRootViewComponent} from "./equipment-root-view/equipment-root-view.component";

const routes: Routes = [
  {
    path: '',
    component: EquipmentRootViewComponent,
    resolve: {},
    children: [
      {path: '', pathMatch: 'full', redirectTo: 'all'},
      {
        path: 'all',
        component: EquipmentListComponent,
        resolve: {},
      },
      {
        path: ':equipmentId',
        component: EquipmentDetailsComponent,
        resolve: {}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EquipmentRoutingModule {
}
