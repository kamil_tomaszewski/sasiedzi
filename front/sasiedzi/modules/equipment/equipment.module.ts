import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {EquipmentRoutingModule} from './equipment-routing.module';
import {EquipmentDetailsComponent} from "./equipment-details/equipment-details.component";
import {EquipmentRootViewComponent} from "./equipment-root-view/equipment-root-view.component";
import {EquipmentListComponent} from "./equipment-list/equipment-list.component";
import {SharedModuleModule} from "../shared-module/shared-module.module";


@NgModule({
  declarations: [
    EquipmentRootViewComponent,
    EquipmentListComponent,
    EquipmentDetailsComponent
  ],
  imports: [
    CommonModule,
    EquipmentRoutingModule,
    SharedModuleModule
  ]
})
export class EquipmentModule {
}
