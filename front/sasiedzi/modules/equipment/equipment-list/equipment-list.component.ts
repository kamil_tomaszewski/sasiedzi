import {Component, OnInit} from '@angular/core';
import {EquipmentHttpService} from "../../../services/equipment-http.service";
import {Equipment} from "../../../interfaces/equipment";
import {SharedService} from "../../../services/shared.service";
import {Person} from "../../../interfaces/person";
import {EquipmentDataService} from "../../../services/equipment-data.service";

@Component({
  selector: 'app-equipment-list',
  templateUrl: './equipment-list.component.html',
  styleUrls: ['./equipment-list.component.scss']
})
export class EquipmentListComponent implements OnInit {

  myEquipments: Equipment[] = [];
  otherEquipments: Equipment[] = [];
  loggedUser: Person | undefined;

  constructor(private equipmentDataService: EquipmentDataService,
              private equipmentHttpService: EquipmentHttpService,
              private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.loggedUser = this.sharedService.loggedUser
    this.equipmentHttpService.getEquipmentForUser(this.loggedUser!.id)
      .subscribe(({myEquipments, otherEquipments}) => {
        console.info(myEquipments);
        console.info(otherEquipments);
        localStorage.setItem('equipments', JSON.stringify(myEquipments.concat(otherEquipments)));
        this.myEquipments = myEquipments;
        this.otherEquipments = otherEquipments;
      })

  }
}
