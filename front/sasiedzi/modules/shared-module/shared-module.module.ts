import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TopbarComponent } from './topbar/topbar.component';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule, MomentDateAdapter } from "@angular/material-moment-adapter";
import { PersonTileComponent } from './person-tile/person-tile.component';


@NgModule({
  declarations: [
    TopbarComponent,
    SidemenuComponent,
    PersonTileComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule, 
  ],
  exports: [
    RouterModule,
    FormsModule,
    TranslateModule,
    ReactiveFormsModule,
    TopbarComponent,
    SidemenuComponent,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule, 
    PersonTileComponent
  ]
})
export class SharedModuleModule { }
