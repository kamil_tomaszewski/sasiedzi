import { Component, OnInit } from '@angular/core';
import { Person } from 'interfaces/person';
import { PeopleDataServiceService } from 'services/people-data-service.service';
import { SharedService } from 'services/shared.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  loggedUser: Person | undefined;

  constructor(
    private sharedService: SharedService
  ) { }

  ngOnInit(): void {
    //TODO: id mock;
    this.loggedUser = this.sharedService.loggedUser;
  }

}
