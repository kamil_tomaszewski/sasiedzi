import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

  sideMenuLinks = [
    { id: 0, icon:'menu.png', url: '/dashboard', name: 'DASHBOARD' },
    { id: 1, icon:'group.png', url: '/people', name: 'PEOPLE' },
    { id: 2, icon:'event.png', url: '/events', name: 'EVENTS' },
    { id: 3, icon:'responsive.png', url: '/equipments', name: 'EQUIPMENTS' }
  ]

  constructor() { }

  ngOnInit(): void {
  }

}
