import { Component, Input, OnInit } from '@angular/core';
import { Person } from 'interfaces/person';

@Component({
  selector: 'app-person-tile',
  templateUrl: './person-tile.component.html',
  styleUrls: ['./person-tile.component.scss']
})
export class PersonTileComponent implements OnInit {

  @Input()
  person!: Person; 
  @Input()
  hideLabels!: boolean;

  constructor() { }

  ngOnInit(): void {
  }

}
