import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventDetailsComponent } from './event-details/event-details.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventRootViewComponent } from './event-root-view/event-root-view.component';
import { NewEventFormComponent } from './new-event-form/new-event-form.component';

const routes: Routes = [
  {
    path: '',
    component: EventRootViewComponent,
    resolve: {

    },
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'all' },
      {
        path: 'all',
        component: EventListComponent,
        resolve: {},
      },
      {
        path: 'add',
        component: NewEventFormComponent,
        resolve: {}
      },
      {
        path: ':eventId',
        component: EventDetailsComponent,
        resolve: {}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EventRoutingModule { }
