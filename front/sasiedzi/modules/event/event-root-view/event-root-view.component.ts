import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { EventDataService } from 'services/event-data.service';

@Component({
  selector: 'app-event-root-view',
  templateUrl: './event-root-view.component.html',
  styleUrls: ['./event-root-view.component.scss']
})
export class EventRootViewComponent implements OnInit {

  routeDataSubscribe: Subscription = new Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private eventDataService: EventDataService) { }

  ngOnInit(): void {
    this.routeDataSubscribe = this.activatedRoute.data.subscribe((data) => {
      this.eventDataService.eventList = data.events;
      localStorage.setItem('companyEvents', JSON.stringify(data));
    });
  }
}