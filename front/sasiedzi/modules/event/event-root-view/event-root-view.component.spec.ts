import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EventRootViewComponent } from './event-root-view.component';

describe('EventRootViewComponent', () => {
  let component: EventRootViewComponent;
  let fixture: ComponentFixture<EventRootViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EventRootViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EventRootViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
