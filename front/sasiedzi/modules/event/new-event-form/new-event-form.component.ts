import { Component, OnInit } from '@angular/core';
import {EventDataService} from '../../../services/event-data.service';
import {AddEventCommand} from '../../../interfaces/add-event-command';
import {SharedService} from '../../../services/shared.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-event-form',
  templateUrl: './new-event-form.component.html',
  styleUrls: ['./new-event-form.component.scss']
})
export class NewEventFormComponent implements OnInit {
  eventType: string = 'INTEGRATION_OUTPUT';

  newEvent: any = {
    name: '',
    description: '',
    eventType: this.eventType,
    startDate: null,
    endDate: null,
    address: {}
  }

  constructor(
    private eventDataService: EventDataService,
    private sharedService: SharedService,
    private router: Router) { }

  ngOnInit(): void {
  }

  createEvent() {
    if (this.sharedService.loggedUser?.id){
      this.eventDataService.createEvent({
        type: this.newEvent.eventType,
        creationUserId: this.sharedService.loggedUser?.id,
        description: this.newEvent.description,
        startDate: this.newEvent.startDate,
        endDate: this.newEvent.endDate,
        address: this.newEvent.address
      } as AddEventCommand).subscribe((data) => {
        this.eventDataService.eventList = data;
        this.navigateToEventsList();
      });
    }
  }

  navigateToEventsList() {
    this.router.navigate(['/events/all'])
  }

}
