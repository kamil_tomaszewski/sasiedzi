import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EventRoutingModule } from './event-routing.module';
import { EventRootViewComponent } from './event-root-view/event-root-view.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventDetailsComponent } from './event-details/event-details.component';
import { SharedModuleModule } from 'modules/shared-module/shared-module.module';
import { NewEventFormComponent } from './new-event-form/new-event-form.component';


@NgModule({
  declarations: [
    EventRootViewComponent,
    EventListComponent,
    EventDetailsComponent,
    NewEventFormComponent
  ],
  imports: [
    CommonModule,
    EventRoutingModule,
    SharedModuleModule
  ]
})
export class EventModule { }
