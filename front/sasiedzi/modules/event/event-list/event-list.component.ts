import { Component, OnInit } from '@angular/core';
import { CompanyEvent } from 'interfaces/event';
import { Person } from 'interfaces/person';
import { EventDataService } from 'services/event-data.service';
import { EventHttpService } from 'services/event-http.service';
import { PeopleHttpService } from 'services/people-http.service';
import { SharedService } from 'services/shared.service';
import { AddEventCommand } from '../../../interfaces/add-event-command';
import { UpdateEventCommand } from '../../../interfaces/update-event-command';

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

  events: CompanyEvent[] | null = [];
  otherEvents: CompanyEvent[] | null | undefined = [];
  loggedUserEvents: CompanyEvent[] | null | undefined = [];

  constructor(private eventDataService: EventDataService,
    private sharedService: SharedService,
    private eventHttpService: EventHttpService) { }

  ngOnInit(): void {
    this.events = this.eventDataService.eventList;
    localStorage.setItem('companyEvents', JSON.stringify(this.events));

    this.getOtherUserEvents();
    this.getLoggedUserEvents();
  }

  getLoggedUserEvents() {
    if (!!this.sharedService.loggedUser) {
      this.loggedUserEvents = this.events?.filter(companyEvent =>
        companyEvent.personIds?.some(personId => personId === this.sharedService.loggedUser?.id));
    }
  }

  getOtherUserEvents() {
    if (!!this.sharedService.loggedUser) {
      const personId = Number(this.sharedService.loggedUser?.id);
      this.otherEvents = this.events?.filter((companyEvent) => {
        return !companyEvent.personIds?.includes(personId);
      })
    }
  }

  updateEvent(command: UpdateEventCommand) {
    this.eventHttpService.updateEvent(command).subscribe((data: CompanyEvent[]) => {
      this.events = data;
      this.eventDataService.eventList = data;
    })
  }

  acceptEvent(eventId: number) {
    this.eventHttpService.acceptEvent(eventId).subscribe((data: CompanyEvent[]) => {
      this.events = data;
      this.eventDataService.eventList = data;
    })
  }

  delete(eventId: number) {
    this.eventHttpService.delete(eventId).subscribe((data: CompanyEvent[]) => {
      this.events = data;
      this.eventDataService.eventList = data;
    })
  }
}
