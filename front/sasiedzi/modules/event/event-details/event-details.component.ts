import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CompanyEvent } from 'interfaces/event';
import { Person } from 'interfaces/person';
import { Subscription } from 'rxjs';
import { EventDataService } from 'services/event-data.service';
import { PeopleDataServiceService } from 'services/people-data-service.service';
import { PeopleHttpService } from 'services/people-http.service';
import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {
  routeDataSubscribe: Subscription = new Subscription;
  eventId: number | null;
  userId: number | null;
  eventDetails: CompanyEvent | undefined;
  userExistInEvent: boolean = false;
  peopleInEvent: Person[] = []
  comments: { id: string, user: Person, value: string }[] = []

  constructor(
    private router: Router,
    private eventDataService: EventDataService,
    private activatedRoute: ActivatedRoute,
    private peopleDataService: PeopleDataServiceService,
    private peopleHttp: PeopleHttpService,
    private sharedService: SharedService) {
    this.eventId = null;
    this.userId = null;
  }

  ngOnInit(): void {
    this.routeDataSubscribe = this.activatedRoute.paramMap.subscribe(paramMap => {
      // @ts-ignore
      this.userId = this.sharedService.loggedUser?.id;

      // @ts-ignore - params exist in paramMap
      console.info('eventId', paramMap.params.eventId);
      // @ts-ignore - params exist in paramMap
      this.eventId = Number(paramMap.params.eventId);
      this.eventDetails = this.eventDataService.findEventById(this.eventId);
      this.getCommentsSection();
      if (this.eventDetails) {
        this.getEventUsers(this.eventDetails?.personIds);
      }
      if (this.userId) {
        // @ts-ignore
        this.userExistInEvent = this.isUserSubscribedToEvent(this.userId, this.eventDetails)
      }
    });
  }

  navigateToEventsList() {
    this.router.navigate(['/events/all'])
  }

  getCommentsSection() {
    this.eventDetails?.comments.forEach((comment) => {

      console.log('comment:', comment)
      // @ts-ignore
      const commentAuthor = this.peopleDataService.findPersonById(comment.userId);
      const commentObj = {
        id: comment.id,
        user: commentAuthor,
        value: comment.value
      }
      // @ts-ignore
      this.comments.push(commentObj);
    })
  }

  addUserToEvent(eventId: number) {
    if (!!this.sharedService.loggedUser) {
      this.eventDataService.addUserToEvent(eventId, this.sharedService.loggedUser?.id).subscribe((data) => {
        this.eventDataService.eventList = data;
        this.navigateToEventsList();
      });
    }
  }

  getEventUsers(eventPeopleIds: number[]) {
    this.peopleHttp.getUsers().subscribe((people) => {
      this.peopleInEvent = people.filter(p => eventPeopleIds.includes(p.id));
    });
  }

  isUserSubscribedToEvent(userId: number, event: CompanyEvent) {
    return event.personIds.includes(userId);
  }

  removeUserFromEvent(eventId: number) {
    if (!!this.sharedService.loggedUser) {
      this.eventDataService.removeUserFromEvent(eventId, this.sharedService.loggedUser?.id).subscribe((data) => {
        this.eventDataService.eventList = data;
        this.navigateToEventsList();
      });
    }
  }

  addCommentToEvent(eventId: number, commentValue: string) {
    if (!!this.sharedService.loggedUser) {
      this.eventDataService.addCommentToEvent(eventId, this.sharedService.loggedUser.id, commentValue);
    }
  }

  updateCommentFromEvent(eventId: number, commentValue: string) {
    if (!!this.sharedService.loggedUser) {
      this.eventDataService.updateCommentFromEvent(eventId, this.sharedService.loggedUser.id, commentValue);
    }
  }

  deleteCommentFromEvent(eventId: number) {
    if (!!this.sharedService.loggedUser) {
      this.eventDataService.deleteCommentFromEvent(eventId, this.sharedService.loggedUser.id);
    }
  }
}
