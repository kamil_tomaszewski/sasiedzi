import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from 'interfaces/person';
import { Subscription } from 'rxjs';
import { PeopleDataServiceService } from 'services/people-data-service.service';

@Component({
  selector: 'app-people-profile',
  templateUrl: './people-profile.component.html',
  styleUrls: ['./people-profile.component.scss']
})
export class PeopleProfileComponent implements OnInit, OnDestroy {

  routeDataSubscribe: Subscription = new Subscription;
  personId: number | null;
  personDetails: Person | undefined;
  

  constructor(
    private peopleDataService: PeopleDataServiceService,
    private activatedRoute: ActivatedRoute) {
    this.personId = null;
    
  }



  ngOnInit(): void {
    this.routeDataSubscribe = this.activatedRoute.paramMap.subscribe(paramMap => {
      // @ts-ignore - params exist in paramMap
      console.info('personId',paramMap.params.personId);
      // @ts-ignore - params exist in paramMap
      this.personId = Number(paramMap.params.personId);
      this.personDetails = this.peopleDataService.findPersonById(this.personId);
    });
  }

  ngOnDestroy(): void {
    this.routeDataSubscribe?.unsubscribe();
  }

}

