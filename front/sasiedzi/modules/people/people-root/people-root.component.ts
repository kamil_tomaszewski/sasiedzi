import { Component, OnInit } from '@angular/core';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {PeopleDataServiceService} from '../../../services/people-data-service.service';

@Component({
  selector: 'app-people-root',
  templateUrl: './people-root.component.html',
  styleUrls: ['./people-root.component.scss']
})
export class PeopleRootComponent implements OnInit {

  routeDataSubscribe: Subscription = new Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private peopleDataService: PeopleDataServiceService) { }

  ngOnInit(): void {
    this.routeDataSubscribe = this.activatedRoute.data.subscribe((data) => {
      this.peopleDataService.peopleList = data.events;
      localStorage.setItem('people', JSON.stringify(data));
    });
  }

}
