import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeopleRootComponent } from './people-root.component';

describe('PeopleRootComponent', () => {
  let component: PeopleRootComponent;
  let fixture: ComponentFixture<PeopleRootComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PeopleRootComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PeopleRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
