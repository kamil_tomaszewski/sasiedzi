import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeopleListComponent } from './people-list/people-list.component';
import { PeopleProfileComponent } from './people-profile/people-profile.component';
import { PeopleRootComponent } from './people-root/people-root.component';

const routes: Routes = [
  {
    path: '',
    component: PeopleRootComponent,
    resolve: {

    },
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'all' },
      {
        path: 'all',
        component: PeopleListComponent,
        resolve: {},
      },
      {
        path: ':personId',
        component: PeopleProfileComponent,
        resolve: {}
      }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule { }
