import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PeopleRoutingModule } from './people-routing.module';
import { PeopleRootComponent } from './people-root/people-root.component';
import { PeopleListComponent } from './people-list/people-list.component';
import { PeopleProfileComponent } from './people-profile/people-profile.component';
import { SharedModuleModule } from 'modules/shared-module/shared-module.module';


@NgModule({
  declarations: [
    PeopleRootComponent,
    PeopleListComponent,
    PeopleProfileComponent
  ],
  imports: [
    CommonModule,
    PeopleRoutingModule,
    SharedModuleModule
  ]
})
export class PeopleModule { }
