import {Component, OnInit} from '@angular/core';
import {Person} from 'interfaces/person';
import {PeopleDataServiceService} from 'services/people-data-service.service';
import {PeopleHttpService} from 'services/people-http.service';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {

  people: Person[] | null = [];

  constructor(private peopleDataService: PeopleDataServiceService,
              private peopleHttpService: PeopleHttpService) {
  }

  ngOnInit(): void {
    if (this.people === null || this.people.length <= 0) {
      this.peopleHttpService.getUsers().subscribe((data: Person[]) => {
        console.info(data);
        this.people = data;
        localStorage.setItem('people', JSON.stringify(data));
        this.peopleDataService.peopleList = data;
      })
    }
  }
}
