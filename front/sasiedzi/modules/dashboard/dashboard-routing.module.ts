import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardRootViewComponent } from './dashboard-root-view/dashboard-root-view.component';

const routes: Routes = [
  {
    path: '', component: DashboardRootViewComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
