import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardRootViewComponent } from './dashboard-root-view/dashboard-root-view.component';
import { SharedModuleModule } from 'modules/shared-module/shared-module.module';


@NgModule({
  declarations: [
    DashboardRootViewComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModuleModule
  ]
})
export class DashboardModule { }
