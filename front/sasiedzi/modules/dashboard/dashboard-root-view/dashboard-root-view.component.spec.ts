import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardRootViewComponent } from './dashboard-root-view.component';

describe('DashboardRootViewComponent', () => {
  let component: DashboardRootViewComponent;
  let fixture: ComponentFixture<DashboardRootViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardRootViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardRootViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
