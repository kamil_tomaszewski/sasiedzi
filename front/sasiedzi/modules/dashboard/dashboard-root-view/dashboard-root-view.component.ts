import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-root-view',
  templateUrl: './dashboard-root-view.component.html',
  styleUrls: ['./dashboard-root-view.component.scss']
})
export class DashboardRootViewComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
