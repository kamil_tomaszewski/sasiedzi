import {Address} from './address';

export interface AddEventCommand {
  type: string;
  creationUserId: number;
  description: string;
  startDate: string;
  endDate: string;
  address: Address;
}
