export interface Comment {
  id: string;
  userId: string;
  value: string;
}
