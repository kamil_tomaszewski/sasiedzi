export interface Equipment {
  id: number,
  code: string,
  description: string,
  value: string,
  type: string,
  property: string,
  status: string,
  currentOwnerId: number


}
