import {Project} from "./project";
import {CompanyEvent} from "./event";
import {Equipment} from "./equipment";

export interface Person {
  id: number,
  firstname: string,
  lastname: string,
  email: string,
  profession: string,
  photo: string,
  technologies: string[]
  superuser: boolean
  projects: Project[]
  events: CompanyEvent[]
  equipments: Equipment[]
}
