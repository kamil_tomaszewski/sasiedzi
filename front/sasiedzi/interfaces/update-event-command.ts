import {Address} from './address';

export interface UpdateEventCommand {
  eventId: string;
  type: string;
  description: string;
  startDate: string;
  endDate: string;
  address: Address;
}
