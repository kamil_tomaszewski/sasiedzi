export interface Project {
  id: number,
  company: string,
  project: string,
}
