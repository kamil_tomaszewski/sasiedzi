export interface CreateUserCommand {
  firstname: string;
  lastname: string;
  email: string;
  profession: string;
}
