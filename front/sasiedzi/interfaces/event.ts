import {Address} from "./address";
import {Comment} from "./comment";

export interface CompanyEvent {
  id: number,
  type: string,
  name: string,
  description: string,
  creationDate: string,
  createdBy: string,
  startDate: string,
  endDate: string,
  address: Address,
  status: string,
  personIds: number[],
  comments: Comment[],
}
