export interface AddEquipmentCommand {
  userId: string;
  code: string;
  description: string;
  value: string;
  type: string;
  property: string;
}
