export interface UpdateEquipmentCommand {
  equipmentId: string;
  code: string;
  description: string;
  value: string;
  type: string;
  property: string;
}
