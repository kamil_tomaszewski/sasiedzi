export interface Address {
  street: string,
  homeNo: string,
  flatNo: string,
  city: string,
  postCode: string,
  postCity: string,
  country: string
}
