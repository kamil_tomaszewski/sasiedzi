package pl.decerto.sasiedzi.project;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.decerto.sasiedzi.user.User;
import pl.decerto.sasiedzi.user.UserRepo;
import pl.decerto.sasiedzi.utils.ResourceHelper;

import java.util.List;

@Configuration
class ProjectReader {

    @Bean
    ProjectRepo projectRepo(ResourceHelper resourceHelper) {
        ProjectRepo repo = new ProjectRepo();
        List<Project> projects = resourceHelper.readJsonFileAsList("classpath:projects.json", Project.class);
        repo.saveAll(projects);
        return repo;
    }


}
