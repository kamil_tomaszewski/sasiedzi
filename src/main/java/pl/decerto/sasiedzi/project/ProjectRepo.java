package pl.decerto.sasiedzi.project;

import pl.decerto.sasiedzi.utils.InMemoryRepo;

import java.util.concurrent.atomic.AtomicLong;

public class ProjectRepo extends InMemoryRepo<Project, Long> {

    private final AtomicLong idSequence = new AtomicLong(1000);

    @Override
    public <S extends Project> S save(S entity) {
        if (entity.getId() == null) {
            entity.setId(idSequence.incrementAndGet());
        }
        entities.put(entity.getId(), entity);
        return entity;
    }
}
