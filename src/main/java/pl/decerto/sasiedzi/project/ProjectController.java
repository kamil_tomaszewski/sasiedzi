package pl.decerto.sasiedzi.project;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@AllArgsConstructor
@RestController
@RequestMapping("api/project")
class ProjectController {

    private final ProjectFacade facade;

    @GetMapping()
    Collection<Project> getAllProjects() {
        return facade.getAllProjects();
    }

    @GetMapping("user/{userId}")
    Collection<Project> getAllProjects( @PathVariable Long userId) {
        return facade.getProjectsByUserId(userId);
    }

    @PostMapping()
    Project save(@RequestBody CreateProjectCommand command) {
        return facade.save(command);
    }

    @PutMapping("{projectId}/user/{userId}")
    Project addUserToProject(@PathVariable Long projectId, @PathVariable Long userId) {
        return facade.addUserToProject(projectId, userId);
    }

    @DeleteMapping("{projectId}/user/{userId}")
    void removeUserFromProject(@PathVariable Long projectId, @PathVariable Long userId) {
        facade.removeUserFromProject(projectId, userId);
    }

    @DeleteMapping("{id}")
    void delete(@PathVariable Long id) {
        facade.delete(id);
    }
}
