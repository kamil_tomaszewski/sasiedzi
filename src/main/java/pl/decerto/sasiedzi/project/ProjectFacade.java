package pl.decerto.sasiedzi.project;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decerto.sasiedzi.user.User;
import pl.decerto.sasiedzi.user.UserRepo;

import java.util.Collection;

@Component
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class ProjectFacade {

    private final ProjectRepo repo;
    private final UserRepo userRepo;

    Collection<Project> getAllProjects() {
        return repo.findAll();
    }

    Collection<Project> getProjectsByUserId(Long userId) {
        return userRepo.findByIdOrExplode(userId)
                .getProjects();
    }

    Project save(CreateProjectCommand command) {
        Project domain = toDomain(command);
        return repo.save(domain);
    }

    private Project toDomain(CreateProjectCommand command) {
        return Project.builder()
                .company(command.getCompany())
                .project(command.getProject())
                .build();
    }

    void delete(Long id) {
        Project Project = repo.findByIdOrExplode(id);
        Project.getPersons()
                .forEach(p -> p.removeProject(Project));
        repo.delete(Project);
    }

    Project addUserToProject(Long projectId, Long userId) {
        Project project = repo.findByIdOrExplode(projectId);
        User user = userRepo.findByIdOrExplode(userId);
        user.addProject(project);
        return project;
    }

    void removeUserFromProject(Long projectId, Long userId) {
        Project project = repo.findByIdOrExplode(projectId);
        User user = userRepo.findByIdOrExplode(userId);
        user.removeProject(project);
    }
}
