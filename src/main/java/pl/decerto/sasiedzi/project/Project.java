package pl.decerto.sasiedzi.project;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;
import pl.decerto.sasiedzi.user.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Data
@Builder
public class Project {
    private Long id;
    private String company;
    private String project;

    @JsonIgnore
    private Collection<User> persons;

    public Project addPerson(User user) {
        persons.add(user);
        return this;
    }

    public Project removePerson(User user) {
        persons.remove(user);
        return this;
    }

    public Collection<User> getPersons() {
        if (Objects.isNull(persons)) {
            persons = new ArrayList<>();
        }
        return persons;
    }
}
