package pl.decerto.sasiedzi.equipment;

public enum EquipmentProperty {
	MOVEABLE,
	IMMOVABLE
}