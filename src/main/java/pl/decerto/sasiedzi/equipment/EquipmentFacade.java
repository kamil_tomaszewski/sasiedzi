package pl.decerto.sasiedzi.equipment;

import lombok.Data;
import org.springframework.stereotype.Component;
import pl.decerto.sasiedzi.user.User;
import pl.decerto.sasiedzi.user.UserRepo;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

@Component
@Data
public class EquipmentFacade {
    private final EquipmentRepo equipmentRepo;
    private final UserRepo userRepo;

    public Collection<Equipment> getAllEquipment() {
        return equipmentRepo.findAll();
    }

    public EquipmentResource getAllEquipment(long userId) {
        List<Equipment> all = equipmentRepo.findAll();
        List<Equipment> myEquipments = all
                .stream()
                .filter(a -> a.getCurrentOwnerIdOptional()
                        .map(i -> i.equals(userId))
                        .orElse(false))
                .toList();
        List<Equipment> otherEquipments = all
                .stream()
                .filter(a -> !a.getCurrentOwnerIdOptional()
                        .map(i -> i.equals(userId))
                        .orElse(false))
                .toList();
        return EquipmentResource.builder()
                .myEquipments(myEquipments)
                .otherEquipments(otherEquipments)
                .build();
    }

    public Collection<Equipment> assignEquipmentToUser(long equipmentId, long userId) {
        Equipment equipment = equipmentRepo.findByIdOrExplode(equipmentId);
        User user = userRepo.findByIdOrExplode(userId);
        user.addEquipment(equipment);
        equipment.setStatus(Equipment.Status.CONFIRMATION_APPLICATION);
        equipmentRepo.save(equipment);
        return getAllEquipment();
    }

    public Collection<Equipment> returnEquipment(long equipmentId) {
        Equipment equipment = equipmentRepo.findByIdOrExplode(equipmentId);
        equipment.setStatus(Equipment.Status.RETURN_APPLICATION);
        equipmentRepo.save(equipment);
        return getAllEquipment();
    }

    public Collection<Equipment> returnSuperuserEquipment(long equipmentId) {
        Equipment equipment = equipmentRepo.findByIdOrExplode(equipmentId);
        equipment.getCurrentOwnerIdOptional()
                .flatMap(userRepo::findById)
                .ifPresent(u -> u.removeEquipment(equipment));
        equipment.setStatus(Equipment.Status.NEW);
        equipmentRepo.save(equipment);
        return getAllEquipment();
    }

    public Collection<Equipment> createEquipment(AddEquipmentCommand command) {
        Equipment equipment = toDomain(command);
        equipment.setStatus(Equipment.Status.NEW);
        equipmentRepo.save(equipment);
        return getAllEquipment();
    }

    public Collection<Equipment> updateEquipment(UpdateEquipmentCommand command) {
        Equipment equipment = equipmentRepo.findByIdOrExplode(command.getEquipmentId());
        equipment.setCode(command.getCode());
        equipment.setDescription(command.getDescription());
        equipment.setType(command.getType());
        equipment.setProperty(command.getProperty());
        equipment.setValue(new BigDecimal(command.getValue()));
        equipmentRepo.save(equipment);
        return getAllEquipment();
    }

    public Collection<Equipment> acceptEquipment(long equipmentId) {
        Equipment equipment = equipmentRepo.findByIdOrExplode(equipmentId);
        equipment.setStatus(Equipment.Status.CONFIRMATION);
        equipmentRepo.save(equipment);

        return getAllEquipment();
    }

    private Equipment toDomain(AddEquipmentCommand command) {
        Equipment equipment = new Equipment();
        equipment.setCode(command.getCode());
        equipment.setDescription(command.getDescription());
        equipment.setType(command.getType());
        equipment.setProperty(command.getProperty());
        equipment.setValue(new BigDecimal(command.getValue()));
        equipment.setCurrentOwnerId(command.getUserId());
        return equipment;
    }
}
