package pl.decerto.sasiedzi.equipment;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UpdateEquipmentCommand implements Serializable {

    private Long equipmentId;
    private String code;
    private String description;
    private String value;
    private EquipmentType type;
    private EquipmentProperty property;
}
