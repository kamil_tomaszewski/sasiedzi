package pl.decerto.sasiedzi.equipment;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Component;
import pl.decerto.sasiedzi.utils.InMemoryRepo;

public class EquipmentRepo extends InMemoryRepo<Equipment, Long> {

	private final AtomicLong idSequence = new AtomicLong(1000);

	@Override
	public <S extends Equipment> S save(S entity) {
		if (entity.getId() == null) {
			entity.setId(idSequence.incrementAndGet());
		}
		entities.put(entity.getId(), entity);
		return entity;
	}
}
