package pl.decerto.sasiedzi.equipment;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class EquipmentResource {
    List<Equipment> myEquipments;
    List<Equipment> otherEquipments;
}
