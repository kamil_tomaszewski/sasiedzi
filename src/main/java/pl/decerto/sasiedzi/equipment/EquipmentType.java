package pl.decerto.sasiedzi.equipment;

public enum EquipmentType {
	LAPTOP,
	ROUTER,
	MONITOR,
	OTHER
}
