package pl.decerto.sasiedzi.equipment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.sasiedzi.user.User;

import java.math.BigDecimal;
import java.util.Optional;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Equipment {
    Long id;
    String code;
    String description;
    BigDecimal value;
    EquipmentType type;
    EquipmentProperty property;
    Status status;
    Long currentOwnerId;

    Optional<Long> getCurrentOwnerIdOptional() {
        return Optional.ofNullable(currentOwnerId);
    }

    public enum Status {
        NEW, CONFIRMATION_APPLICATION, CONFIRMATION, RETURN_APPLICATION
    }
}
