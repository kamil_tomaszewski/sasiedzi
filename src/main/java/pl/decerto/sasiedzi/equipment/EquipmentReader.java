package pl.decerto.sasiedzi.equipment;

import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.decerto.sasiedzi.utils.ResourceHelper;

@Configuration
class EquipmentReader {

    @Bean
	EquipmentRepo equipmentRepo(ResourceHelper resourceHelper) {
        EquipmentRepo repo = new EquipmentRepo();
        List<Equipment> equipments = resourceHelper.readJsonFileAsList("classpath:equipments.json", Equipment.class);
        repo.saveAll(equipments);
        return repo;
    }

}
