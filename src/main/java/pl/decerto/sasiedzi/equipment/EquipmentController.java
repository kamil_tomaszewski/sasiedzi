package pl.decerto.sasiedzi.equipment;

import lombok.Data;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@Data
@RestController
@RequestMapping("api/equipment")
public class EquipmentController {

    private final EquipmentFacade facade;

    @GetMapping()
    Collection<Equipment> getAllEquipment() {
        return facade.getAllEquipment();
    }

    @GetMapping("user/{userId}")
    EquipmentResource getAllEquipment(@PathVariable long userId) {
        return facade.getAllEquipment(userId);
    }

    @PostMapping()
    Collection<Equipment> createEquipment(@RequestBody AddEquipmentCommand command) {
        return facade.createEquipment(command);
    }

    @PutMapping()
    Collection<Equipment> updateEquipment(@RequestBody UpdateEquipmentCommand command) {
        return facade.updateEquipment(command);
    }

    @PutMapping("{equipmentId}/accept")
    Collection<Equipment> acceptEquipment(@PathVariable long equipmentId) {
        return facade.acceptEquipment(equipmentId);
    }

    @PutMapping("{equipmentId}/user/{userId}")
    Collection<Equipment> assign(@PathVariable long equipmentId, @PathVariable Long userId) {
        return facade.assignEquipmentToUser(equipmentId, userId);
    }

    @PutMapping("{equipmentId}/return")
    Collection<Equipment> returnEquipment(@PathVariable long equipmentId) {
        return facade.returnEquipment(equipmentId);
    }

    @PutMapping("{equipmentId}/return_superuser")
    Collection<Equipment> returnSuperuserEquipment(@PathVariable long equipmentId) {
        return facade.returnSuperuserEquipment(equipmentId);
    }
}
