package pl.decerto.sasiedzi.comment;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Component;
import pl.decerto.sasiedzi.utils.InMemoryRepo;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Component
public class CommentRepo extends InMemoryRepo<Comment, Long> {

    private final AtomicLong idSequence = new AtomicLong(1000);

    @Override
    public <S extends Comment> S save(S entity) {
        if (entity.getId() == null) {
            entity.setId(idSequence.incrementAndGet());
        }
        entities.put(entity.getId(), entity);
        return entity;
    }
}
