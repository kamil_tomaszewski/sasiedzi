package pl.decerto.sasiedzi.comment;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class UpdateCommentCommand implements Serializable {

    private final long commentId;
    private final String value;
}
