package pl.decerto.sasiedzi.comment;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class AddCommentCommand implements Serializable {

    private final long userId;
    private final String value;
}
