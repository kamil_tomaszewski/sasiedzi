package pl.decerto.sasiedzi.comment;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class Comment {

    private Long id;
    private Long userId;
    private String value;
}
