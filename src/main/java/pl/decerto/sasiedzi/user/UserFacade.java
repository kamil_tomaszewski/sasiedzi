package pl.decerto.sasiedzi.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class UserFacade {

    private final UserRepo repo;

    Collection<User> getAllUsers() {
        return repo.findAll();
    }

    User save(CreateUserCommand command) {
        User domain = toDomain(command);
        return repo.save(domain);
    }

    private User toDomain(CreateUserCommand command) {
        return User.builder()
                .firstname(command.getFirstname())
                .lastname(command.getLastname())
                .email(command.getEmail())
                .profession(command.getProfession())
                .build();
    }

    void delete(Long id) {
        User user = repo.findByIdOrExplode(id);
        user.getEvents()
                .forEach(e -> e.removePerson(user));
        user.getProjects()
                .forEach(e -> e.removePerson(user));
        repo.delete(user);
    }
}
