package pl.decerto.sasiedzi.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class CreateUserCommand implements Serializable {

    private String firstname;
    private String lastname;
    private String email;
    private String profession;
}
