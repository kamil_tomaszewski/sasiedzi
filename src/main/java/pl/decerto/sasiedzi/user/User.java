package pl.decerto.sasiedzi.user;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.sasiedzi.equipment.Equipment;
import pl.decerto.sasiedzi.event.Event;
import pl.decerto.sasiedzi.project.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static lombok.AccessLevel.PACKAGE;

@Data
@Builder
public class User {
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    private String profession;
    private String photo;
    private List<String> technologies;
    private boolean superuser;

    @Builder.Default
    private List<Project> projects = List.of();

    @Getter(PACKAGE)
    @Setter(PACKAGE)
    @Builder.Default
    private List<Long> projectIds = List.of();

    @Builder.Default
    private List<Event> events = List.of();

    @Getter(PACKAGE)
    @Setter(PACKAGE)
    @Builder.Default
    private List<Long> eventIds = List.of();

    @Builder.Default
    List<Equipment> equipments = List.of();

    @Getter(PACKAGE)
    @Setter(PACKAGE)
    @Builder.Default
    List<Long> equipmentIds = List.of();

    public User addProject(Project project) {
        getProjects().add(project);
        project.getPersons().add(this);
        return this;
    }

    public User removeProject(Project project) {
        getProjects().remove(project);
        return this;
    }

    public User addEvent(Event event) {
        getEvents().add(event);
        event.addPerson(this);
        return this;
    }

    public User removeEvent(Event event) {
        getEvents().remove(event);
        event.removePerson(this);
        return this;
    }

    public User addEquipment(Equipment equipment) {
        getEquipments().add(equipment);
        equipment.setCurrentOwnerId(getId());
        return this;
    }

    public User removeEquipment(Equipment equipment) {
        getEquipments().remove(equipment);
        equipment.setCurrentOwnerId(null);
        return this;
    }

    public List<Project> getProjects() {
        if (Objects.isNull(projects)) {
            projects = new ArrayList<>();
        }
        return projects;
    }

    public List<Event> getEvents() {
        if (Objects.isNull(events)) {
            events = new ArrayList<>();
        }
        return events;
    }

    public List<Equipment> getEquipments() {
        if (Objects.isNull(equipments)) {
            equipments = new ArrayList<>();
        }
        return equipments;
    }
}
