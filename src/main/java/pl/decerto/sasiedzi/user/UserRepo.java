package pl.decerto.sasiedzi.user;

import pl.decerto.sasiedzi.utils.InMemoryRepo;

import java.util.concurrent.atomic.AtomicLong;

public class UserRepo extends InMemoryRepo<User, Long> {

    private final AtomicLong idSequence = new AtomicLong(1000);

    @Override
    public <S extends User> S save(S entity) {
        if (entity.getId() == null) {
            entity.setId(idSequence.incrementAndGet());
        }
        entities.put(entity.getId(), entity);
        return entity;
    }
}
