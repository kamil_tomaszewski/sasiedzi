package pl.decerto.sasiedzi.user;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@AllArgsConstructor
@RestController
@RequestMapping("api/user")
class UserController {

    private final UserFacade facade;

    @GetMapping()
    Collection<User> getAllUsers() {
        return facade.getAllUsers();
    }

    @PostMapping()
    User save(@RequestBody CreateUserCommand command) {
        return facade.save(command);
    }

    @DeleteMapping("{id}")
    void delete(@PathVariable Long id) {
        facade.delete(id);
    }
}
