package pl.decerto.sasiedzi.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.decerto.sasiedzi.equipment.Equipment;
import pl.decerto.sasiedzi.equipment.EquipmentRepo;
import pl.decerto.sasiedzi.event.Event;
import pl.decerto.sasiedzi.event.EventRepo;
import pl.decerto.sasiedzi.project.Project;
import pl.decerto.sasiedzi.project.ProjectRepo;
import pl.decerto.sasiedzi.utils.ResourceHelper;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Configuration
class UserReader {

    @Bean
    UserRepo userRepo(
            ResourceHelper resourceHelper,
            ProjectRepo projectRepo,
            EventRepo eventRepo,
            EquipmentRepo equipmentRepo) {
        UserRepo userRepo = new UserRepo();
        List<User> users = resourceHelper.readJsonFileAsList("classpath:users.json", User.class);
        users.forEach(u -> {
            getProjects(projectRepo, u).forEach(u::addProject);
            getEvents(eventRepo, u).forEach(u::addEvent);
            getEquipments(equipmentRepo, u).forEach(u::addEquipment);
        });
        userRepo.saveAll(users);
        return userRepo;
    }

    private List<Project> getProjects(ProjectRepo repo, User u) {
        if (Objects.isNull(u.getProjectIds())) {
            u.setProjectIds(List.of());
        }
        return u.getProjectIds()
                .stream()
                .map(repo::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private List<Event> getEvents(EventRepo repo, User u) {
        if (Objects.isNull(u.getEventIds())) {
            u.setEventIds(List.of());
        }
        return u.getEventIds()
                .stream()
                .map(repo::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    private List<Equipment> getEquipments(EquipmentRepo repo, User u) {
        if (Objects.isNull(u.getEquipmentIds())) {
            u.setEquipmentIds(List.of());
        }
        return u.getEquipmentIds()
                .stream()
                .map(repo::findById)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }
}
