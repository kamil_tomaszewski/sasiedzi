package pl.decerto.sasiedzi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SasiedziApplication {

	public static void main(String[] args) {
		SpringApplication.run(SasiedziApplication.class, args);
	}

}
