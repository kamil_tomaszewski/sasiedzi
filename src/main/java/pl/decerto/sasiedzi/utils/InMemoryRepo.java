package pl.decerto.sasiedzi.utils;

import org.springframework.util.Assert;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("PMD.GenericsNaming")
public abstract class InMemoryRepo<T, ID> {

    private static final String ENTITIES_MUST_NOT_BE_NULL = "Entities must not be null!";
    private static final String ID_MUST_NOT_BE_NULL = "The given id must not be null!";
    private static final String IDS_MUST_NOT_BE_NULL = "Ids must not be null!";
    private static final String ENTITY_MUST_NOT_BE_NULL = "Entity must not be null!";

    protected final ConcurrentHashMap<ID, T> entities = new ConcurrentHashMap<>();

    public abstract <S extends T> S save(S entity);

    public <S extends T> List<S> saveAll(Iterable<S> entities) {
        Assert.notNull(entities, ENTITIES_MUST_NOT_BE_NULL);
        List<S> result = new ArrayList<>();

        for (final S entity : entities) {
            result.add(save(entity));
        }

        return result;
    }

    public Optional<T> findById(ID id) {
        Assert.notNull(id, ID_MUST_NOT_BE_NULL);
        return Optional.ofNullable(entities.get(id));
    }

    public T findByIdOrExplode(ID id) {
        return findById(id)
            .orElseThrow(() -> new IllegalStateException(String.format("Entity not found by id %s", id)));
    }

    public boolean existsById(ID id) {
        Assert.notNull(id, ID_MUST_NOT_BE_NULL);
        return findById(id).isPresent();
    }

    public List<T> findAll() {
        return new ArrayList<>(entities.values());
    }

    public List<T> findAllById(Iterable<ID> ids) {
        Assert.notNull(ids, IDS_MUST_NOT_BE_NULL);
        Iterator<ID> iterator = ids.iterator();

        if (!iterator.hasNext()) {
            return Collections.emptyList();
        } else {
            List<T> results = new ArrayList<>();

            while (iterator.hasNext()) {
                ID id = iterator.next();
                findById(id).ifPresent(results::add);
            }

            return results;
        }
    }

    public long count() {
        return entities.size();
    }

    public void deleteById(ID id) {
        Assert.notNull(id, ID_MUST_NOT_BE_NULL);
        delete(findById(id)
                .orElseThrow());
    }

    public void delete(T entity) {
        Assert.notNull(entity, ENTITY_MUST_NOT_BE_NULL);
        entities.values()
                .remove(entity);
    }

    public void deleteAll(Iterable<? extends T> entities) {
        Assert.notNull(entities, ENTITIES_MUST_NOT_BE_NULL);

        for (final T entity : entities) {
            delete(entity);
        }
    }

    public void deleteAll() {
        entities.clear();
    }

}
