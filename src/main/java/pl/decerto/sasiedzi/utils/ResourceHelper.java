package pl.decerto.sasiedzi.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
@RequiredArgsConstructor
public class ResourceHelper {
    private final ResourceLoader loader;
    private final ObjectMapper MAPPER;

    public <T> List<T> readJsonFileAsList(@NonNull String resourcePath, @NonNull Class<T> type) {
        final Resource resource = loader.getResource(resourcePath);
        MAPPER.registerModule(new JavaTimeModule());
        MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        final CollectionType listType = MAPPER.getTypeFactory().constructCollectionType(List.class, type);
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(resource.getInputStream(), UTF_8))) {
            return MAPPER.readValue(reader, listType);
        } catch (IOException ex) {
            throw new IllegalStateException("Can't load data from resource: " + resourcePath, ex);
        }
    }
}
