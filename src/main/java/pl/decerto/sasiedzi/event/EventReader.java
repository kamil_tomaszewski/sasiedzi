package pl.decerto.sasiedzi.event;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.decerto.sasiedzi.project.Project;
import pl.decerto.sasiedzi.project.ProjectRepo;
import pl.decerto.sasiedzi.utils.ResourceHelper;

import java.util.List;

@Configuration
class EventReader {

    @Bean
    EventRepo eventRepo(ResourceHelper resourceHelper) {
        EventRepo repo = new EventRepo();
        List<Event> projects = resourceHelper.readJsonFileAsList("classpath:events.json", Event.class);
        repo.saveAll(projects);
        return repo;
    }


}
