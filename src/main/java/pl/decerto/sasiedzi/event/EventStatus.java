package pl.decerto.sasiedzi.event;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
enum EventStatus {

    NEW,
    ACCEPTED;
}
