package pl.decerto.sasiedzi.event;

import java.util.Collection;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.decerto.sasiedzi.comment.AddCommentCommand;
import pl.decerto.sasiedzi.comment.UpdateCommentCommand;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@AllArgsConstructor
@RestController
@RequestMapping("api/event")
class EventController {

    private final EventFacade facade;

    @GetMapping()
    Collection<Event> getAllEvents() {
        return facade.getAllEvents();
    }

    @PostMapping()
    Collection<Event> save(@RequestBody AddEventCommand command) {
        return facade.save(command);
    }

    @PutMapping()
    Collection<Event> update(@RequestBody UpdateEventCommand command) {
        return facade.update(command);
    }

    @PutMapping("{eventId}/accept")
    Collection<Event> acceptEvent(@PathVariable Long eventId) {
        return facade.acceptEvent(eventId);
    }

    @PutMapping("{eventId}/user/{userId}")
    Collection<Event> addUserToEvent(@PathVariable Long eventId, @PathVariable Long userId) {
        return facade.addUserToEvent(eventId, userId);
    }

    @DeleteMapping("{eventId}/user/{userId}")
    Collection<Event> removeUserFromEvent(@PathVariable Long eventId, @PathVariable Long userId) {
        return facade.removeUserFromEvent(eventId, userId);
    }

    @PostMapping("{eventId}/comment")
    Collection<Event> addCommentToEvent(@PathVariable Long eventId, @RequestBody AddCommentCommand command) {
        return facade.addCommentToEvent(eventId, command);
    }

    @PutMapping("{eventId}/comment")
    Collection<Event> updateCommentFromEvent(@PathVariable Long eventId, @RequestBody UpdateCommentCommand command) {
        return facade.updateCommentFromEvent(eventId, command);
    }

    @PutMapping("{eventId}/comment/{commentId}")
    Collection<Event> deleteCommentFromEvent(@PathVariable Long eventId, @PathVariable Long commentId) {
        return facade.removeCommentFromEvent(eventId, commentId);
    }

    @DeleteMapping("{id}")
    Collection<Event> delete(@PathVariable Long id) {
        return facade.delete(id);
    }
}
