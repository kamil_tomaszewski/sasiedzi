package pl.decerto.sasiedzi.event;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import pl.decerto.sasiedzi.comment.Comment;
import pl.decerto.sasiedzi.user.User;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
public class Event {
    private Long id;
    private EventType type;
    private EventStatus status = EventStatus.NEW;
    private String name;
    private String description;
    private LocalDateTime creationDate;
    private Long createdBy;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Address address;
    private List<Comment> comments = new ArrayList<>();
    private Collection<Long> personIds = new ArrayList<>();

    @JsonIgnore
    private Collection<User> persons = new ArrayList<>();

    public Event addPerson(User user) {
        persons.add(user);
        personIds.add(user.getId());
        return this;
    }

    public Event addComment(Comment comment) {
        comments.add(comment);
        return this;
    }
    
    public Event removePerson(User user) {
        persons.remove(user);
        personIds.remove(user.getId());
        return this;
    }

    public Event removeComment(Comment comment) {
        comments.remove(comment);
        return this;
    }

    public Event removeComment(Long commentId) {
        comments.removeIf(comment -> comment.getId().equals(commentId));
        return this;
    }

    public Comment findComment(Long commentId) {
        return comments.stream()
            .filter(comment -> comment.getId().equals(commentId))
            .findAny()
            .orElseThrow(() -> new IllegalStateException("Not found comment with id: " + commentId));
    }

    public Event updateComment(Long commentId, String commentValue) {
        Comment commentToUpdate = findComment(commentId);
        int indexOfCommentToUpdate = comments.indexOf(commentToUpdate);
        commentToUpdate.setValue(commentValue);
        comments.add(indexOfCommentToUpdate, commentToUpdate);
        return this;
    }

    public Collection<User> getPersons() {
        if (Objects.isNull(persons)) {
            persons = new ArrayList<>();
        }
        return persons;
    }

    public List<Comment> getComments() {
        if (Objects.isNull(comments)) {
            comments = new ArrayList<>();
        }
        return comments;
    }
}
