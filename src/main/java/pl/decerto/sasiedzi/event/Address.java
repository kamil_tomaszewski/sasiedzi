package pl.decerto.sasiedzi.event;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Address {

    private final String street;
    private final String homeNo;
    private final String flatNo;
    private final String city;
    private final String postCode;
    private final String postCity;
    private final String country;
}
