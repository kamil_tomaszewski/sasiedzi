package pl.decerto.sasiedzi.event;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import pl.decerto.sasiedzi.comment.AddCommentCommand;
import pl.decerto.sasiedzi.comment.Comment;
import pl.decerto.sasiedzi.comment.CommentRepo;
import pl.decerto.sasiedzi.comment.UpdateCommentCommand;
import pl.decerto.sasiedzi.user.User;
import pl.decerto.sasiedzi.user.UserRepo;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Comparator;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Component
@AllArgsConstructor(access = AccessLevel.PACKAGE)
public class EventFacade {

    private final EventRepo repo;
    private final UserRepo userRepo;
    private final CommentRepo commentRepo;

    Collection<Event> getAllEvents() {
        return repo.findAll().stream()
                .sorted(Comparator.comparing(Event::getId))
                .toList();
    }

    Collection<Event> save(AddEventCommand command) {
        Event domain = toDomain(command);
        repo.save(domain);
        return getAllEvents();
    }

    Collection<Event> update(UpdateEventCommand command) {
        Event event = repo.findByIdOrExplode(command.getEventId());
        event.setType(command.getType());
        event.setName(command.getName());
        event.setDescription(command.getDescription());
        event.setStartDate(command.getStartDate());
        event.setEndDate(command.getEndDate());
        event.setAddress(command.getAddress());
        repo.save(event);
        return getAllEvents();
    }

    Collection<Event> delete(Long id) {
        Event event = repo.findByIdOrExplode(id);
        event.getPersons()
                .forEach(p -> p.removeEvent(event));
        repo.delete(event);
        return getAllEvents();
    }

    Collection<Event> addUserToEvent(Long eventId, Long userId) {
        Event event = repo.findByIdOrExplode(eventId);
        User user = userRepo.findByIdOrExplode(userId);
        user.addEvent(event);
        repo.save(event);
        return getAllEvents();
    }

    Collection<Event> removeUserFromEvent(Long eventId, Long userId) {
        Event event = repo.findByIdOrExplode(eventId);
        User user = userRepo.findByIdOrExplode(userId);
        user.removeEvent(event);
        repo.save(event);
        return getAllEvents();
    }

    Collection<Event> addCommentToEvent(Long eventId, AddCommentCommand command) {
        Event event = repo.findByIdOrExplode(eventId);
        Comment comment = toDomain(command);
        comment = commentRepo.save(comment);
        event.addComment(comment);
        repo.save(event);
        return getAllEvents();
    }

    Collection<Event> removeCommentFromEvent(Long eventId, Long commentId) {
        Event event = repo.findByIdOrExplode(eventId);
        Comment comment = event.findComment(commentId);
        commentRepo.delete(comment);
        event.removeComment(commentId);
        repo.save(event);
        return getAllEvents();
    }

    Collection<Event> updateCommentFromEvent(Long eventId, UpdateCommentCommand command) {
        Event event = repo.findByIdOrExplode(eventId);
        event.updateComment(command.getCommentId(), command.getValue());
        repo.save(event);
        return getAllEvents();
    }

    Collection<Event> acceptEvent(Long eventId) {
        Event event = repo.findByIdOrExplode(eventId);
        event.setStatus(EventStatus.ACCEPTED);
        repo.save(event);
        return getAllEvents();
    }

    private Event toDomain(AddEventCommand command) {
        Event event = new Event();
        event.setType(EventType.valueOf(command.getType()));
        event.setCreatedBy(command.getCreationUserId());
        event.setCreationDate(LocalDateTime.now());
        event.setStartDate(command.getStartDate());
        event.setEndDate(command.getEndDate());
        event.setName(command.getName());
        event.setDescription(command.getDescription());
        event.setAddress(command.getAddress());
        return event;
    }

    private Comment toDomain(AddCommentCommand command) {
        Comment comment = new Comment();
        comment.setUserId(command.getUserId());
        comment.setValue(command.getValue());
        return comment;
    }

}
