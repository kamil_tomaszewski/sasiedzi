package pl.decerto.sasiedzi.event;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
class UpdateEventCommand implements Serializable {

    private Long eventId;
    private EventType type;
    private String name;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime endDate;
    private Address address;
}
