package pl.decerto.sasiedzi.event;

/**
 * @author Tomasz Gładki <tomasz.gladki@nn.pl>
 */
enum EventType {

    TRIP,
    INTEGRATION_OUTPUT,
    INDOOR_PARTY,
    OTHER;
}
