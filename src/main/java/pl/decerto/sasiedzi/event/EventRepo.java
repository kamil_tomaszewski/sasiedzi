package pl.decerto.sasiedzi.event;

import pl.decerto.sasiedzi.utils.InMemoryRepo;

import java.util.concurrent.atomic.AtomicLong;

public class EventRepo extends InMemoryRepo<Event, Long> {

    private final AtomicLong idSequence = new AtomicLong(1000);

    @Override
    public <S extends Event> S save(S entity) {
        if (entity.getId() == null) {
            entity.setId(idSequence.incrementAndGet());
        }
        entities.put(entity.getId(), entity);
        return entity;
    }
}
